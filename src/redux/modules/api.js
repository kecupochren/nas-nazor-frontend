import { combineReducers } from 'redux'

import common from './api/common'
import questions from './api/questions'
import posts from './api/posts'
import passwords from './api/passwords'
import users from './api/users'
import misc from './api/misc'

// Export Actions
/* eslint-disable */
export * as passwords from './api/passwords'
export * as users from './api/users'
export * as misc from './api/misc'
/* eslint-enable */

export default combineReducers({
  common,
  questions,
  posts,
  passwords,
  misc,
  users
})
