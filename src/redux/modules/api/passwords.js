const PASSWORD_RESET = 'api/posts/PASSWORD_RESET'
const PASSWORD_RESET_SUCCESS = 'api/posts/PASSWORD_RESET_SUCCESS'
const PASSWORD_RESET_FAIL = 'api/posts/PASSWORD_RESET_FAIL'

const PUT_USERS_EMAIL = 'api/posts/PUT_USERS_EMAIL'

const initialState = {
  loading: false,
  usersEmail: null
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case PASSWORD_RESET:
      return {
        ...state,
        loading: true
      }
    case PASSWORD_RESET_SUCCESS: {
      return {
        ...state,
        loading: false
      }
    }
    case PASSWORD_RESET_FAIL:
      return {
        ...state,
        loading: false
      }

    // ----------------------------------------------

    case PUT_USERS_EMAIL:
      return {
        ...state,
        usersEmail: action.email
      }

    default:
      return state
  }
}

/**
 * Actions
 */
export const passwordReset = data => ({
  types: [PASSWORD_RESET, PASSWORD_RESET_SUCCESS, PASSWORD_RESET_FAIL],
  promise: client =>
    client.post('api/passwordReset', {
      data
    })
})

export const putUsersEmail = email => ({
  type: PUT_USERS_EMAIL,
  email
})
