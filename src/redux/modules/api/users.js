const ACTIVATE_ACCOUNT = 'api/posts/ACTIVATE_ACCOUNT'
const ACTIVATE_ACCOUNT_SUCCESS = 'api/posts/ACTIVATE_ACCOUNT_SUCCESS'
const ACTIVATE_ACCOUNT_FAIL = 'api/posts/ACTIVATE_ACCOUNT_FAIL'

const initialState = {
  loading: true,
  isActivated: false,
  error: null,
  email: null
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case ACTIVATE_ACCOUNT:
      return {
        ...state,
        loading: true
      }
    case ACTIVATE_ACCOUNT_SUCCESS: {
      return {
        ...state,
        isActivated: true,
        loading: false,
        email: action.result.email
      }
    }
    case ACTIVATE_ACCOUNT_FAIL: {
      let error = ''

      switch (action.error.statusCode) {
        case 404:
          error = 'Autorizačný kód je neplatný'
          break
        case 409:
          error = 'Užívateľ je už aktivovaný'
          break
        default:
          error = 'Pri aktivácii sa vyskytla neznáma chyba'
      }

      return {
        ...state,
        loading: false,
        error
      }
    }
    // ----------------------------------------------

    default:
      return state
  }
}

/**
 * Actions
 */
export const activateAccount = token => {
  return {
    types: [ACTIVATE_ACCOUNT, ACTIVATE_ACCOUNT_SUCCESS, ACTIVATE_ACCOUNT_FAIL],
    promise: client => client.post(`api/users/activation/${token}`)
  }
}
