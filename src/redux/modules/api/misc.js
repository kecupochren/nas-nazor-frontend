const GET_TWITTER_POST_DATA = 'api/misc/GET_TWITTER_POST_DATA'
const GET_TWITTER_POST_DATA_SUCCESS = 'api/misc/GET_TWITTER_POST_DATA_SUCCESS'
const GET_TWITTER_POST_DATA_FAIL = 'api/misc/GET_TWITTER_POST_DATA_FAIL'

const GET_URL_META_DATA = 'api/misc/GET_URL_META_DATA'
const GET_URL_META_DATA_SUCCESS = 'api/misc/GET_URL_META_DATA_SUCCESS'
const GET_URL_META_DATA_FAIL = 'api/misc/GET_URL_META_DATA_FAIL'

const initialState = {
  getTwitterPostData: {
    loading: false,
    loaded: false,
    error: null,
    data: null
  },
  getUrlMetaData: {
    loading: false,
    loaded: false,
    error: null,
    data: null
  }
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_TWITTER_POST_DATA:
      return {
        ...state,
        getTwitterPostData: {
          ...state.getTwitterPostData,
          loading: true,
          loaded: false,
          error: null,
          data: null
        }
      }
    case GET_TWITTER_POST_DATA_SUCCESS:
      return {
        ...state,
        getTwitterPostData: {
          ...state.getTwitterPostData,
          loading: false,
          loaded: true,
          data: action.result.data
        }
      }
    case GET_TWITTER_POST_DATA_FAIL:
      return {
        ...state,
        getTwitterPostData: {
          ...state.getTwitterPostData,
          loading: false,
          error: action.error
        }
      }

    // ----------------------------------------

    case GET_URL_META_DATA:
      return {
        ...state,
        getUrlMetaData: {
          ...state.getUrlMetaData,
          loading: true,
          loaded: false,
          error: null,
          data: null
        }
      }
    case GET_URL_META_DATA_SUCCESS:
      return {
        ...state,
        getUrlMetaData: {
          ...state.getUrlMetaData,
          loading: false,
          loaded: true,
          data: action.result.data
        }
      }
    case GET_URL_META_DATA_FAIL:
      return {
        ...state,
        getUrlMetaData: {
          ...state.getUrlMetaData,
          loading: false,
          error: action.error
        }
      }

    default:
      return state
  }
}

export const getTwitterPostData = postId => ({
  types: [
    GET_TWITTER_POST_DATA,
    GET_TWITTER_POST_DATA_SUCCESS,
    GET_TWITTER_POST_DATA_FAIL
  ],
  promise: client => client.get(`api/socialPosts/twitter/${postId}`)
})

export const getUrlMetaData = url => ({
  types: [GET_URL_META_DATA, GET_URL_META_DATA_SUCCESS, GET_URL_META_DATA_FAIL],
  promise: client => client.get(`api/socialPosts/url/${url}`)
})
