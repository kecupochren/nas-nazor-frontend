import React, { Component } from 'react'
import { connect } from 'react-redux'

import { users as usersApi } from 'redux/modules/api'

function decorator(config = {}) {
  return ComposedComponent => {
    @connect(
      ({ api }) => ({
        users: api.users
      }),
      { ...usersApi }
    )
    class Users extends Component {
      static propTypes = {
        activateAccount: React.PropTypes.func.isRequired,
        params: React.PropTypes.object.isRequired
      }

      static defaultProps = {
        params: {}
      }

      componentDidMount() {
        config.activateAccount && this.handleActivateAccount()
      }

      handleActivateAccount = () => {
        const { params } = this.props

        if (params.authToken) {
          this.props.activateAccount(params.authToken)
        }
      }

      render() {
        return <ComposedComponent {...this.props} />
      }
    }
    return Users
  }
}

export default decorator
