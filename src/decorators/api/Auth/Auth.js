import React, { Component } from 'react'
import { get } from 'lodash'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { notification } from 'redux/modules/app'
import { passwords as passwordsApi } from 'redux/modules/api'
import { schema } from 'models/password-reset'

function decorator() {
  return ComposedComponent => {
    @connect(
      ({ api }) => ({
        passwords: api.passwords
      }),
      { ...passwordsApi, notification, push }
    )
    class PasswordReset extends Component {
      static propTypes = {
        notification: React.PropTypes.func.isRequired,
        push: React.PropTypes.func.isRequired,
        passwordReset: React.PropTypes.func.isRequired,
        putUsersEmail: React.PropTypes.func.isRequired
      }

      handlePasswordReset = (fields, authToken) => {
        this.props
          .passwordReset(schema(fields, authToken))
          .then(({ body: { email } }) => {
            this.props.putUsersEmail(email)
            this.props.push('/nastavenie-hesla/success')
          })
          .catch((err = {}) => {
            if (get(err, 'body.statusCode') === 422) {
              this.props.notification('Autorizačný kód je neplatný', 'error')
            } else {
              this.props.notification(get(err, 'body.error'), 'error')
            }
            console.log('err', err)
          })
      }

      render() {
        return (
          <ComposedComponent
            {...this.props}
            onPasswordReset={this.handlePasswordReset}
          />
        )
      }
    }
    return PasswordReset
  }
}

export default decorator
