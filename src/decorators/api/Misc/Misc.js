import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import isUrl from 'is-url'

import { misc as miscApi } from 'redux/modules/api'

function decorator() {
  return ComposedComponent => {
    @connect(
      ({ api }) => ({
        misc: api.misc
      }),
      { ...miscApi }
    )
    class Misc extends Component {
      static propTypes = {
        getTwitterPostData: PropTypes.func.isRequired,
        misc: PropTypes.shape({
          getTwitterPostData: PropTypes.object.isRequired,
          getUrlMetaData: PropTypes.object.isRequired
        }).isRequired
      }

      handleFetchUrlData = async (inputUrl, postType) => {
        if (inputUrl && isUrl(inputUrl)) {
          switch (postType) {
            case 'article': {
              const url = encodeURIComponent(inputUrl)

              return this.props.getUrlMetaData(url).then(res => res.body)
            }

            case 'twitter': {
              const postId = inputUrl.split('/').reverse()[0]

              return this.props.getTwitterPostData(postId).then(res => res.body)
            }

            default:
              break
          }
        }
      }

      render() {
        const { misc } = this.props

        const isFetchingUrlData =
          misc.getTwitterPostData.loading || misc.getUrlMetaData.loading

        return (
          <ComposedComponent
            {...this.props}
            handleFetchUrlData={this.handleFetchUrlData}
            isFetchingUrlData={isFetchingUrlData}
          />
        )
      }
    }
    return Misc
  }
}

export default decorator
