import React, { Component } from 'react'
import Helmet from 'react-helmet'

function decorator(config) {
  return ComposedComponent => {
    class Meta extends Component {
      render() {
        const { title, description = '', image = '' } = config

        const meta = [
          {
            name: 'viewport',
            content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
          },
          { 'http-equiv': 'X-UA-Compatible', content: 'IE=edge,chrome=1' },
          { charset: 'utf-8' },
          { property: 'site_name', content: 'náš.názor' },
          { property: 'og:image', content: image },
          { property: 'og:locale', content: 'en_US' },
          {
            property: 'og:title',
            content: 'náš.názor'
          },
          { property: 'og:description', content: description },
          { property: 'og:type', content: 'website' },
          { property: 'og:url', content: '' },
          { property: 'twitter:card', content: 'summary' },
          { property: 'twitter:site', content: '@' },
          { property: 'twitter:creator', content: '@' },
          { property: 'twitter:title', content: title },
          { property: 'twitter:description', content: description },
          { property: 'twitter:image', content: image },
          { property: 'twitter:image:width', content: '200' },
          { property: 'twitter:image:height', content: '200' }
        ]

        return (
          <div>
            <Helmet
              title={'náš.názor - ' + title}
              titleTemplate={'náš.názor - ' + title}
              defaultTitle="náš.názor"
              meta={meta}
            />
            <ComposedComponent {...this.props} />
          </div>
        )
      }
    }
    return Meta
  }
}

export default decorator
