export default function(model) {
  const data = {}

  data.author = model.author
  data.title = model.title
  data.contentUrl = model.contentUrl
  data.imageUrl = model.imageUrl || null
  data.authorImageUrl = model.authorImageUrl || null

  data.postTypeId = model.postTypeId.value

  data.cities = model.cities || []
  data.tags = model.tags || []

  data.order = (model.order && Number(model.order)) || 0

  return data
}
