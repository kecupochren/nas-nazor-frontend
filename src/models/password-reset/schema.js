export default function(model, authToken) {
  const data = {}

  data.password = model.password
  data.token = authToken

  return data
}
