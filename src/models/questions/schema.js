export default function(model) {
  const data = {}

  data.author = model.author
  data.title = model.title
  data.text = model.text
  data.effect = model.effect
  data.effectNote = model.effectNote
  data.availableTo = model.availableTo

  data.cities = model.cities || []
  data.tags = model.tags || []
  data.posts =
    (model.posts &&
      model.posts.map(post => ({
        id: post.id,
        title: post.title
      }))) ||
    []

  data.order = (model.order && Number(model.order)) || 0

  // lol
  data.questionTypeId = 'single'
  data.questionOptions = [
    { title: 'Áno' },
    { title: 'Nie' },
    { title: 'Neviem' }
  ]

  return data
}
