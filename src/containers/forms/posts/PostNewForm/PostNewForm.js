import React, { Component } from 'react'
import { get } from 'lodash'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import {
  Input,
  Textarea,
  SelectCity,
  SelectTags,
  Select,
  Checkbox
} from 'components/fields'
import { Button, LineDivider } from 'components/misc'
import { Form } from 'components/layout'

import { form } from 'decorators'
import { apiPosts, apiMisc } from 'decorators/api'
import { POST_TYPES } from 'constants/enums'

import validate from './post-new-form.validation'

const LABEL_WIDTH = '150px'
const FORM_NAME = 'postNew'

@connect(() => ({
  initialValues: {
    author: 'Admin',
    postTypeId: POST_TYPES.article
  }
}))
@form({
  form: FORM_NAME,
  validate
})
@apiPosts()
@apiMisc()
export default class PostNewForm extends Component {
  static propTypes = {
    handleCreatePost: PropTypes.func.isRequired,
    handleFetchUrlData: PropTypes.func.isRequired,
    change: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    isFetchingUrlData: PropTypes.bool.isRequired,
    posts: PropTypes.object.isRequired,
    values: PropTypes.object.isRequired
  }

  static defaultProps = {
    values: {}
  }

  handleFetchUrlData = () =>
    // Timeout to catch pasted content
    setTimeout(() => {
      const { values } = this.props
      const { contentUrl, postTypeId } = values

      this.props
        .handleFetchUrlData(contentUrl, postTypeId.value)
        .then(this.handleLoadUrlData)
    }, 50)

  handleLoadUrlData = data => {
    const title = get(data, 'title') || get(data, 'full_text')
    const image =
      get(data, 'image.url') ||
      get(data, 'extended_entities.media[0].media_url')
    const author = get(data, 'user.name')
    const authorImageUrl = get(data, 'user.profile_image_url')

    title && this.props.change('title', title)
    image && this.props.change('imageUrl', image)
    author && this.props.change('author', author)
    authorImageUrl && this.props.change('authorImageUrl', authorImageUrl)
  }

  render() {
    const {
      handleSubmit,
      handleCreatePost,
      isFetchingUrlData,
      posts,
      values
    } = this.props

    const { submitting } = posts
    const { postTypeId } = values

    return (
      <Form wide onSubmit={handleSubmit(handleCreatePost)}>
        <Select
          searchable={false}
          clearable={false}
          inline
          inlineLabelWidth={LABEL_WIDTH}
          label="Typ článku"
          name="postTypeId"
          placeholder=""
          options={POST_TYPES.list}
        />

        <Input
          inline
          inlineLabelWidth={LABEL_WIDTH}
          label="Link na článok"
          name="contentUrl"
          loading={isFetchingUrlData}
          onBlur={this.handleFetchUrlData}
          onPaste={this.handleFetchUrlData}
          required
        />

        <Textarea
          disableLineBreaks
          inline
          inlineLabelWidth={LABEL_WIDTH}
          label="Text (nadpis)"
          name="title"
          titleStyle
          required
        />

        <Input
          inline
          inlineLabelWidth={LABEL_WIDTH}
          label="Link na obrázok"
          name="imageUrl"
          // required
        />

        <LineDivider />

        <Input
          inline
          inlineLabelWidth={LABEL_WIDTH}
          label="Autor"
          name="author"
          required
        />

        {postTypeId &&
          postTypeId.value !== 'article' &&
          <Input
            inline
            inlineLabelWidth={LABEL_WIDTH}
            label="Link na obr. autora"
            name="authorImageUrl"
          />}

        <SelectCity
          inline
          inlineLabelWidth={LABEL_WIDTH}
          label="Región"
          name="cities"
        />

        <SelectTags
          inline
          inlineLabelWidth={LABEL_WIDTH}
          label="Tagy"
          name="tags"
        />

        <Checkbox
          inline
          inlineLabelWidth={LABEL_WIDTH}
          label="Hlavný článok"
          name="order"
          isNumber
        />

        <Button
          isLoading={submitting}
          label="Vytvoriť článok"
          offsetLeft={LABEL_WIDTH}
          type="submit"
        />
      </Form>
    )
  }
}
