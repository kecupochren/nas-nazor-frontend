import React, { Component } from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'

import { LineDivider, Button } from 'components/misc'
import {
  SelectTags,
  SelectCity,
  Select,
  Input,
  Textarea,
  Checkbox
} from 'components/fields'
import { Form } from 'components/layout'

import { form } from 'decorators'
import { apiPosts, apiMisc } from 'decorators/api'
import { POST_TYPES } from 'constants/enums'

const FORM_NAME = 'postEdit'

@apiPosts({
  detail: true
})
@apiMisc()
@form({
  form: FORM_NAME,
  initialProps: 'posts.detail',
  initialPropsMapper: props => {
    return {
      ...props,
      postTypeId: POST_TYPES[props.postTypeId]
    }
  }
})
export default class PostDetailForm extends Component {
  static propTypes = {
    handleEditPost: PropTypes.func.isRequired,
    handleDeletePost: PropTypes.func.isRequired,
    handleFetchUrlData: PropTypes.func.isRequired,
    change: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    posts: PropTypes.object.isRequired,
    isFetchingUrlData: PropTypes.bool.isRequired,
    values: PropTypes.object.isRequired
  }

  static defaultProps = {
    values: {}
  }

  handleFetchUrlData = () =>
    // Timeout to catch pasted content
    setTimeout(() => {
      const { values } = this.props
      const { contentUrl, postTypeId } = values

      this.props
        .handleFetchUrlData(contentUrl, postTypeId.value)
        .then(this.handleLoadUrlData)
    }, 50)

  handleLoadUrlData = data => {
    const title = get(data, 'title') || get(data, 'full_text')
    const image =
      get(data, 'image.url') ||
      get(data, 'extended_entities.media[0].media_url')
    const author = get(data, 'user.name')
    const authorImageUrl = get(data, 'user.profile_image_url')

    title && this.props.change('title', title)
    image && this.props.change('imageUrl', image)
    author && this.props.change('author', author)
    authorImageUrl && this.props.change('authorImageUrl', authorImageUrl)
  }

  render() {
    const { values, isFetchingUrlData } = this.props

    const { postTypeId } = values

    const loading = get(this.props.posts, 'detailLoading')

    return (
      <Form
        onSubmit={this.props.handleSubmit(this.props.handleEditPost)}
        loading={loading}
      >
        <Select
          searchable={false}
          clearable={false}
          label="Typ článku"
          name="postTypeId"
          placeholder=""
          options={POST_TYPES.list}
        />

        <Textarea
          disableLineBreaks
          label="Text (nadpis)"
          name="title"
          titleStyle
        />

        <Input
          label="Link na článok"
          name="contentUrl"
          loading={isFetchingUrlData}
          onBlur={this.handleFetchUrlData}
          onPaste={this.handleFetchUrlData}
        />

        <Input label="Link na obrázok" name="imageUrl" />

        <LineDivider />

        <Input label="Autor" name="author" />

        {postTypeId &&
          postTypeId.value !== 'article' &&
          <Input label="Link na obrázok autora" name="authorImageUrl" />}

        <SelectCity label="Región" name="cities" />

        <SelectTags label="Tagy" name="tags" />

        <Checkbox label="Hlavný článok" name="order" isNumber />

        <Button block type="submit" label="Upraviť" />

        <Button
          danger
          block
          onClick={this.props.handleDeletePost}
          label="Zmazať článok"
        />
      </Form>
    )
  }
}
