import React, { Component } from 'react'
import { get } from 'lodash'
import config from 'config'
import PropTypes from 'prop-types'

import { Button } from 'components/misc'
import {
  DatePicker,
  SelectTags,
  SelectCity,
  SelectPosts,
  Input,
  Textarea,
  Checkbox
} from 'components/fields'
import { Form } from 'components/layout'
import { QuestionDetailResults } from 'components/sections/questions'

import { form } from 'decorators'
import { apiQuestions } from 'decorators/api'

import validate from './question-detail-form.validation'

const FORM_NAME = 'questionEdit'

@apiQuestions({
  detail: true
})
@form({
  form: FORM_NAME,
  initialProps: 'questions.detail',
  validate
})
export default class QuestionDetailForm extends Component {
  static propTypes = {
    handleUpdateQuestion: PropTypes.func.isRequired,
    handleDeleteQuestion: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    questions: PropTypes.object.isRequired
  }

  handleGetQuestionExcelFile = () => {
    const { questions } = this.props
    const questionId = questions.detail.id

    window.open(`${config.API_URL}/reports/question-${questionId}.zip`)
  }

  handleNormalizeResults = () => {
    const { questions } = this.props

    const data = get(questions, 'detail.questionOptions')

    if (data) {
      const answers = data.sort((a, b) => a.selectedCount < b.selectedCount)

      const votes = answers.reduce((sum, { selectedCount }) => {
        return (sum += selectedCount)
      }, 0)

      if (votes > 0) {
        let calculatedTotal = 0

        answers.forEach(({ selectedCount }, index) => {
          let result = 0

          if (index === answers.length - 1) {
            result = 100 - calculatedTotal
          } else {
            result = selectedCount ? Math.round(selectedCount * 100 / votes) : 0
          }

          calculatedTotal += result

          answers[index].selectedPercentage = result
        })
      }

      return answers
    }
  }

  render() {
    const { handleSubmit, handleUpdateQuestion } = this.props

    const loading = get(this.props, 'questions.detailLoading')

    return (
      <Form onSubmit={handleSubmit(handleUpdateQuestion)} loading={loading}>
        <Textarea disableLineBreaks label="Text (nadpis)" name="title" titleStyle />

        <Textarea descriptionStyle label="Popis" name="text" />

        <hr />

        <QuestionDetailResults results={this.handleNormalizeResults()} />

        <hr />

        <Input label="Autor" name="author" />

        <DatePicker label="Koniec hlasovania" placeholder="Dátum" name="availableTo" />

        <SelectCity label="Región" name="cities" />

        <hr />

        <Textarea label="Meníme Slovensko" name="effect" />

        <Textarea label="Doplňujúci popis" name="effectNote" />

        <SelectTags label="Tagy" name="tags" />

        <SelectPosts label="Súvisiace články" name="posts" />

        <Checkbox label="Poslať notifikáciu" name="helpers.sendNotification" />

        <Checkbox label="Hlavná otázka" name="order" isNumber />

        <Button block type="submit" label="Upraviť" />

        <Button success block onClick={this.handleGetQuestionExcelFile} label="Stiahnuť Excel" />

        <Button danger block onClick={this.props.handleDeleteQuestion} label="Zmazať otázku" />
      </Form>
    )
  }
}
