import React, { Component, PropTypes } from 'react'
import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import { Input } from 'components/fields'
import { Form } from 'components/layout'
import { Button, Title } from 'components/misc'
import { apiAuth } from 'decorators/api'
import validate from './password-reset.validation.js'

@withRouter
@apiAuth()
@reduxForm({
  form: 'password-reset-form',
  validate
})
export default class PasswordResetForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    onPasswordReset: PropTypes.func.isRequired,
    passwords: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired
  }

  static defaultProps = {
    params: {}
  }

  render() {
    const { handleSubmit, passwords, params } = this.props

    return (
      <Form
        noOffsetTop
        onSubmit={handleSubmit(fields =>
          this.props.onPasswordReset(fields, params.authToken)
        )}
      >
        <Title h2 className="text--center">Nastavenie hesla</Title>

        <div className="base-margin--double">
          <Input
            name="password"
            type="password"
            label="Nové heslo"
            placeholder="Nové heslo"
          />

          <Input
            name="passwordAgain"
            type="password"
            label="Nové heslo znovu"
            placeholder="Nové heslo znovu"
          />
        </div>

        <Button
          center
          wide
          type="submit"
          label="Nastaviť"
          className="base-margin--top"
          isLoading={passwords.loading}
        />
      </Form>
    )
  }
}
