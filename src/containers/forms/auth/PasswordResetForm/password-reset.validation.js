import { required, minLength } from 'utils/validation'

export default function(values) {
  const errors = {}

  errors.password = required(values.password) || minLength(6)(values.password)
  errors.passwordAgain =
    required(values.passwordAgain) || minLength(6)(values.passwordAgain)

  if (values.password !== values.passwordAgain) {
    errors.passwordAgain = 'Heslá sa nezhodujú'
  }

  return errors
}
