import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Paragraph } from 'components/layout'

import style from './about.styl'

export default class About extends Component {
  static propTypes = {
    something: PropTypes.any
  }

  render() {
    return (
      <div className={style.wrapper}>
        <Paragraph>
          <b>
            NÁŠ NÁZOR občianske združenie <br />
          </b>
          Slovenskej jednoty 1 <br />
          040 01 Košice <br />
          Slovensko <br />
        </Paragraph>

        <Paragraph>
          <b>Všeobecné otázky:</b> <br />
          <a href="mailto:nasnazorsk@gmail.com">nasnazorsk@gmail.com</a> <br />
          <br />
          <b>Technické otázky:</b> <br />
          <a href="mailto:Mediapoll.cto@gmail.com">Mediapoll.cto@gmail.com</a> <br />
          <br />
          <b>Tel:</b>
          <br /> <a href="tel:+421908789888">+421 908 789 888</a>
        </Paragraph>
      </div>
    )
  }
}
