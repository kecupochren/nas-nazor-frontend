import React, { Component } from 'react'
import t from 'prop-types'
import { Paragraph, Section } from 'components/layout'
import { Button, Title, SuccessIcon } from 'components/misc'

import { apiAuth } from 'decorators/api'

@apiAuth()
export default class PasswordResetSuccess extends Component {
  static propTypes = {
    passwords: t.shape({
      usersEmail: t.string.isRequired
    }).isRequired
  }

  handleRedirectToApp = () => {
    const { passwords: { usersEmail } } = this.props

    location.assign(
      `https://q5k3a.app.goo.gl/?link=http://www.nasnazor.sk/login?email=${usersEmail}&apn=com.nas.nazor&afl=http://www.nasnazor.sk&ibi=com.nas.nazor.NasNazor&ifl=&ius=nasnazor`
    )
  }

  render() {
    return (
      <Section>
        <div>
          <SuccessIcon />
          <Title h2 className="text--center">
            Vaše heslo bolo úspešne zmenené.
          </Title>

          <Paragraph center>
            Pokračujte do aplikácie:
          </Paragraph>
        </div>

        <Button
          center
          type="button"
          label="Pokračovať"
          onClick={this.handleRedirectToApp}
        />
      </Section>
    )
  }
}
