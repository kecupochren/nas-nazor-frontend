import React, { Component } from 'react'
// import  PropTypes } from 'prop-types'
import { PasswordResetForm } from 'containers/forms/auth'
import { meta } from 'decorators'

@meta({
  title: 'Nastavenie hesla'
})
export default class PasswordReset extends Component {
  static propTypes = {}

  render() {
    return <PasswordResetForm {...this.props} />
  }
}
