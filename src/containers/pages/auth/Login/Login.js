import React, { Component } from 'react'
import { LoginForm } from 'containers/forms/auth'
import { meta } from 'decorators'

@meta({
  title: 'Prihlásenie'
})
export default class Login extends Component {
  render() {
    return <LoginForm />
  }
}
