import React, { Component, PropTypes } from 'react'
import { withRouter } from 'react-router'
import { Paragraph, Section } from 'components/layout'
import { Button, Title, SuccessIcon, ErrorIcon } from 'components/misc'
import { apiUsers } from 'decorators/api'

@withRouter
@apiUsers({
  activateAccount: true
})
export default class AccountActivationForm extends Component {
  static propTypes = {
    users: PropTypes.object.isRequired
  }

  static defaultProps = {
    params: {}
  }

  handleRedirectToApp = () => {
    const { users } = this.props

    location.assign(
      `https://q5k3a.app.goo.gl/?link=http://www.nasnazor.sk/login?email=${users.email}&apn=com.nas.nazor&afl=http://www.nasnazor.sk&ibi=com.nas.nazor.NasNazor&ifl=&ius=nasnazor`
    )
  }

  render() {
    const { users } = this.props

    return (
      <Section loading={users.loading}>
        {users.isActivated
          ? <div>
            <SuccessIcon />
            <Title h2 className="text--center">
                Váš účet bol úspešne aktivovaný
              </Title>

            <Paragraph center>
                Pokračujte do aplikácie:
              </Paragraph>

            <div className="row">
              <div className="col-xs-3" />
              <div className="col-xs-6">
                <Button
                  fluid
                  type="button"
                  label="Pokračovať"
                  onClick={this.handleRedirectToApp}
                />
              </div>
              <div className="col-xs-3" />
            </div>
          </div>
          : <div>
            <ErrorIcon />
            <Title h2 className="text--center">
                Aktivácia neúspešná:
              </Title>
            <Paragraph center className="text--danger">
              {users.error}
            </Paragraph>
          </div>}
      </Section>
    )
  }
}
