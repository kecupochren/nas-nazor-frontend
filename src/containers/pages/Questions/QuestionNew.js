import React, { Component } from 'react'
import { QuestionNewForm } from 'containers/forms/questions'
import { meta } from 'decorators'

@meta({
  title: 'Vytvoriť otázku'
})
export default class QuestionNew extends Component {
  render() {
    return <QuestionNewForm {...this.props} />
  }
}
