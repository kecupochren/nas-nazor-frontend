import React, { Component } from 'react'

import { PostNewForm } from 'containers/forms/posts'
import { meta } from 'decorators'

@meta({
  title: 'Vytvoriť článok'
})
export default class PostNew extends Component {
  render() {
    return <PostNewForm {...this.props} />
  }
}
