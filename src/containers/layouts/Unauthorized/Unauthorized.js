import React, { Component, PropTypes } from 'react'
import cx from 'classnames'
import { Logo } from 'components/misc'

import style from './unauthorized.styl'

export default class Unauthorized extends Component {
  static propTypes = {
    children: PropTypes.any,
    routes: PropTypes.array.isRequired
  }

  render() {
    const { routes } = this.props
    const currentRoute = routes[routes.length - 1] || []

    return (
      <div
        className={cx(style.wrapper, {
          [style.resetPasswordWrapper]: currentRoute.isPasswordReset,
          [style.simpleWrapper]: currentRoute.isSimple
        })}
      >
        <div className={style.colLeft} />
        <div className={style.colRight}>
          <div className={style.logo}>
            <Logo large={currentRoute.logoLarge} />
          </div>
          <div className={style.content}>
            {this.props.children}
          </div>
        </div>
      </div>
    )
  }
}
