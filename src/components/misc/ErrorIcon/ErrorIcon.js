import React, { Component } from 'react'

import style from './error-icon.styl'

export default class ErrorIcon extends Component {
  render() {
    return (
      <div className={`${style.wrapper} ${style.animateIcon}`}>
        <span className={style.animateMark}>
          <span className={`${style.line} ${style.lineLeft}`} />
          <span className={`${style.line} ${style.lineRight}`} />
        </span>
      </div>
    )
  }
}
