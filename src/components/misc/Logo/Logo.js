import React, { Component } from 'react'
import cx from 'classnames'
import PropTypes from 'prop-types'

import style from './logo.styl'

export default class Logo extends Component {
  static propTypes = {
    noOffset: PropTypes.bool,
    large: PropTypes.bool
  }

  render() {
    const { large, noOffset } = this.props

    return (
      <div
        className={cx(style.wrapper, {
          [style.noOffset]: noOffset,
          [style.large]: large
        })}
      >
        nas<span className={style.dot}>.</span>nazor
      </div>
    )
  }
}
