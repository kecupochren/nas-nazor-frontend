import React, { Component, PropTypes } from 'react'
import cx from 'classnames'

import style from './success-icon.styl'

export default class SuccessIcon extends Component {
  static propTypes = {
    className: PropTypes.string,
    gutters: PropTypes.bool
  }

  render() {
    const { className, gutters } = this.props

    return (
      <svg
        className={cx(style.wrapper, className, { [style.gutters]: gutters })}
        viewBox="0 0 52 52"
      >
        <circle className={style.circle} cx="26" cy="26" r="25" fill="none" />
        <path
          className={style.check}
          fill="none"
          d="M14.1 27.2l7.1 7.2 16.7-16.8"
        />
      </svg>
    )
  }
}
