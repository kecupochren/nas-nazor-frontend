import React, { Component } from 'react'

import style from './ribbon.styl'

export default class Ribbon extends Component {
  render() {
    return <div className={style.ribbon}>&nbsp;</div>
  }
}
