import React, { Component } from 'react'
import PropTypes from 'prop-types'

import style from './question-detail-results.styl'

export default class QuestionDetailResults extends Component {
  static propTypes = {
    results: PropTypes.array
  }

  render() {
    const { results } = this.props

    const totalCount = results
      ? results.reduce((sum, value) => sum + value.selectedCount, 0)
      : 0

    return (
      <div>
        {results && results.length > 0
          ? <div className={style.wrapper}>
            {results.map(
                ({ id, selectedCount, selectedPercentage, title }) =>
                  id &&
                  <div className={style.result} key={id}>
                    <div className={style.value}>
                      {selectedPercentage}&thinsp;%
                    </div>
                    <div className={style.count}>
                      {selectedCount} hlasov
                    </div>
                    <div className={style.title}>
                      {title}
                    </div>
                  </div>
              )}
          </div>
          : <p className={style.noResults}>
              Na túto otázku ešte nebolo hlasované.
            </p>}

        <div className={style.total}>
          Celkovo hlasovalo {totalCount} ľudí.
        </div>
      </div>
    )
  }
}
