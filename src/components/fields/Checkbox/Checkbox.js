import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import uuid from 'uuid'
import { Field } from 'redux-form'
import { FieldError, FieldLabel } from 'components/fields/__elements__'
import styles from './checkbox.styl'

export default class Checkbox extends Component {
  // Disable eslint here to declare required propTypes for the label
  /* eslint-disable */
  static propTypes = {
    className: PropTypes.string,
    onInputChange: PropTypes.func,
    normalizeOnBlur: PropTypes.func,
    disabled: PropTypes.bool,
    clearable: PropTypes.bool,
    optional: PropTypes.bool,
    required: PropTypes.bool,
    inline: PropTypes.bool,
    inlineLabelWidth: PropTypes.string,
    loading: PropTypes.bool,
    labelless: PropTypes.bool,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    intlKey: PropTypes.string,
    name: PropTypes.string.isRequired,
    normalize: PropTypes.func,
    isNumber: PropTypes.bool
  }
  /* eslint-enable */

  static defaultProps = {
    loading: false,
    optional: false,
    disabled: false,
    clearable: false,
    validate: () => {}
  }

  state = { isFocused: false }

  key = uuid.v1()

  handleChange = (field, e) => {
    field.input.onChange(e.target.checked)
  }

  renderField = field => {
    const {
      disabled,
      clearable,
      inlineLabelWidth,
      placeholder,
      inline,
      isNumber
    } = this.props

    const { meta, input } = field
    const errorClass = meta.touched && meta.error ? 'has-error' : ''
    const disabledClass = disabled ? styles.disabled : ''
    const clearableClass = clearable ? styles.clearable : ''
    const inlineClass = inline ? styles.inline : ''
    const checked = isNumber ? Boolean(input.value) : input.value

    return (
      <div
        className={`${styles.controls} ${errorClass} ${disabledClass} ${clearableClass} ${inlineClass}`}
      >
        <div className={styles.inputWrapper}>
          <input
            className={styles.formControl}
            {...input}
            type="checkbox"
            onChange={e => this.handleChange(field, e)}
            disabled={disabled}
            checked={checked}
            placeholder={placeholder}
            id={this.key}
          />
          <FieldLabel
            {...this.props}
            htmlFor={this.key}
            className={styles.controlLabel}
          />
        </div>

        <FieldError
          inlineLabelWidth={inlineLabelWidth}
          toShow={meta.touched && meta.error}
          message={meta.error}
        />

      </div>
    )
  }

  render() {
    const { className, name, loading } = this.props

    return (
      <div
        className={cx('form-group', {
          [className]: className
        })}
      >
        <Field
          id={name}
          name={name}
          component={this.renderField}
          normalize={this.props.normalize}
          loading={loading}
        />
      </div>
    )
  }
}
