import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Select } from 'components/fields'

import { apiPosts } from 'decorators/api'

@apiPosts()
export default class SelectPosts extends Component {
  static propTypes = {
    searchPosts: PropTypes.func.isRequired
  }

  render = () =>
    <Select
      getOptions={this.props.searchPosts}
      searchable
      labelKey="title"
      valueKey="id"
      multi
      list
      {...this.props}
    />
}
