import React, { Component } from 'react'
import cx from 'classnames'

import { Spinner } from 'components/misc'
import styles from './section.styl'

export default class Section extends Component {
  static propTypes = {
    children: React.PropTypes.any.isRequired,
    loading: React.PropTypes.bool
  }

  render() {
    const { loading } = this.props

    return (
      <div
        className={cx({
          [styles.loading]: loading
        })}
      >
        <div className={styles.content}>
          {this.props.children}
        </div>
        {loading && <div className={styles.spinner}><Spinner /></div>}
      </div>
    )
  }
}
