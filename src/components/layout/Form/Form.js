import React, { Component } from 'react'
import cx from 'classnames'
import PropTypes from 'prop-types'

import style from './form.styl'

export default class Form extends Component {
  static propTypes = {
    children: PropTypes.any,
    mediumWide: PropTypes.bool,
    wide: PropTypes.bool,
    loading: PropTypes.bool,
    noOffsetTop: PropTypes.bool,
    onSubmit: PropTypes.func.isRequired
  }

  render() {
    const { wide, mediumWide, loading, noOffsetTop } = this.props

    return (
      <form
        className={cx(style.wrapper, {
          [style.wide]: wide,
          [style.mediumWide]: mediumWide,
          [style.loading]: loading,
          [style.noOffsetTop]: noOffsetTop
        })}
        onSubmit={this.props.onSubmit}
      >
        {this.props.children}
      </form>
    )
  }
}
