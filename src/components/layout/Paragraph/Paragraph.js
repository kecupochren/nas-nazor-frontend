import React, { Component } from 'react'
import cx from 'classnames'

import style from './paragraph.styl'

export default class Paragraph extends Component {
  static propTypes = {
    children: React.PropTypes.any.isRequired,
    center: React.PropTypes.bool,
    className: React.PropTypes.string,
    offsetTop: React.PropTypes.bool
  }

  render() {
    const { center, className, offsetTop } = this.props

    return (
      <p
        className={cx({
          [className]: className,
          [style.center]: center,
          'base-margin--top': offsetTop
        })}
      >
        {this.props.children}
      </p>
    )
  }
}
