import { Router } from 'express'
const router = new Router()

router.post('/save-user', (req, res) => {
  if (!req.session) {
    console.log('Session is NOT ready')
  }
  req.session.user = req.body
  res.json(req.session.user)
})

router.post('/logout', (req, res) => {
  req.session.destroy()
  return res.json({})
})

router.get('/loadAuth', (req, res) => {
  res.json((req.session && req.session.user) || null)
})

export default router
