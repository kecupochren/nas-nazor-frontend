function enumize(enumDefinition) {
  const byId = {}
  const list = []
  let key
  let temp

  for (key in enumDefinition) {
    if (enumDefinition.hasOwnProperty(key)) {
      temp = Object.assign({}, enumDefinition[key])
      delete temp.id
      temp[key] = key
      list.push({
        label: enumDefinition[key].label,
        value: enumDefinition[key].id,
        key: enumDefinition[key].value
      })
      byId[enumDefinition[key].id] = temp
    }
  }
  enumDefinition.ids = byId
  enumDefinition.list = list
  return enumDefinition
}

exports.POST_TYPES = enumize({
  article: { label: 'Článok z internetu', id: 'article', value: 'article' },
  facebook: { label: 'Facebook', id: 'facebook', value: 'facebook' },
  twitter: { label: 'Twitter', id: 'twitter', value: 'twitter' }
})
